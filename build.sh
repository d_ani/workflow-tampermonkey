#!/bin/bash
set -e # exit on error

# clean up
rm -rf ./dist

# check version tag
if [ -z "$CI_COMMIT_TAG" ]
then
	echo "No commit tag provided"
	exit 10
fi
VERSION=$(echo $CI_COMMIT_TAG | sed 's/^v//')
echo "Create release for $VERSION"

# create destination dir
mkdir -p ./dist

# build files for distribution
sed "s/{{version-placeholder}}/$VERSION/g" ./workflow.js > ./dist/user.js
line="$(grep -n '// ==/UserScript==' ./dist/user.js)"
head -n"${line%:*}" ./dist/user.js > ./dist/meta.js
