// ==UserScript==
// @name         Workflow Tag
// @namespace    https://bierzappfer.de
// @version      {{version-placeholder}}
// @description  Add additonal information in workflow
// @author       Bierzappfer
// @match        https://zeit.spangler-automation.de/*
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @downloadURL  https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/release/dist/user.js?inline=false
// @updateURL    https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/release/dist/meta.js?inline=false
// ==/UserScript==

let version = "{{version-placeholder}}";
console.log("version: " + version);

GM_addStyle(`
body #form1 table tbody tr:last-child:has(td[colspan="2"]) td,
body #form1 table tbody tr:has(td[colspan="2"]) td {
  border-bottom: 2px solid black !important;
  font-weight: bold !important;
}

body #form1 table tbody tr:last-child:has(td[colspan="2"]) td:nth-child(8),
body #form1 table tbody tr:has(td[colspan="2"]) td:nth-child(8) {
  font-weight: bolder !important;
  color: green !important;
}

body table tbody tr th div {
  margin: auto !important;
  background-image: url(https://spangler-automation.de/wp-content/themes/spangler/images/spangler_logo.png) !important;
  background-size: 777px !important;
  background-repeat: no-repeat !important;
  background-position: bottom left !important;
}

body table {
  border-collapse: collapse !important;
}

body table th,
body table td {
  border-collapse: collapse !important;
  padding: 7px !important;
}

body table td.sperre {
  padding: 7px 7px 7px 20px !important;
}`);




function getTimeOfDayFullMinutes(t) {
    return (t.getHours() * 60 + t.getMinutes()) * 60 * 1000;
}

function parseTime(t) {
    let d = new Date();
    let time = t.match(/(\d+)(?::(\d\d))?\s*(p?)/);
    d.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
    d.setMinutes(parseInt(time[2]) || 0);
    return getTimeOfDayFullMinutes(d);
}

function getDate(t) {
    let copy = new Date(t);
    copy.setHours(0);
    copy.setMinutes(0);
    copy.setSeconds(0);
    copy.setMilliseconds(0);
    return copy;
}

function getCurrentTimeOfDay() {
    let now = new Date();
    return getTimeOfDayFullMinutes(now);
}

function toTimeString(t) {
    if (t === undefined || t === null) {
        return null;
    }
    let sign = t < 0 ? '-' : '';
    t = Math.abs(t);
    let hours = Math.floor(t / 3600.0 / 1000);
    let minutes = Math.round(t / 3600.0 / 1000 * 60 % 60);
    return sign + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
}

// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
// source: https://developer.mozilla.org/en-US/docs/Web/XPath/Snippets
function evaluateXPath(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = xpe.createNSResolver(
        aNode.ownerDocument == null
            ? aNode.documentElement
            : aNode.ownerDocument.documentElement);
    let result = xpe.evaluate(aExpr, aNode, nsResolver, XPathResult.ANY_TYPE, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
        found.push(res);
    return found;
}

function getBookingsTable() {
    try {
        return evaluateXPath(document, '//table[.//tr/td/text() = "Kommen" or .//tr/td/text() = "Gehen"]')[0];
    } catch {
        return null;
    }
}

function getMobileOfficeReminder() {
    try {
        return evaluateXPath(document, '//p[text() = "Bitte denke an die Buchung für mobiles Arbeiten auf I-100!"]')[0];
    } catch {
        return null;
    }
}

function getMobileOfficeBooking() {
    try {
        // get the first booking for mobile office (cell must contain "I-100")
        return evaluateXPath(document, '//table//tr/td[1]').slice().map(x => x.innerText).filter(x => x.includes('I-100'))[0];
    } catch {
        return null;
    }
}

function getSelectedDate() {
    try {
        let dateStr = evaluateXPath(document, '//td[.//input/@id = "ORIGINALDATUM"]//input[@id = "TTMMJJJJ"]')[0].value;
        let m = dateStr.match(/(\d+)\.(\d+)\.(\d+)/);
        return new Date(m[3] + '-' + m[2] + '-' + m[1]);
    } catch {
        return null;
    }
}

function getBookings() {
    try {
        let bookings = [];

        let rows = evaluateXPath(getBookingsTable(), './/tr');
        if (rows.length < 2) {
            return null;
        }

        let headerRow = rows[0];
        let timeRow = rows[1];
        let headers = evaluateXPath(headerRow, './td').map(td => td.innerText);
        let times = evaluateXPath(timeRow, './td').map(td => td.innerText);
        if (headers.length !== times.length) {
            return null;
        }

        for (let i = 0; i < headers.length; ++i) {
            bookings.push({
                type: headers[i],
                timeStr: times[i],
                time: parseTime(times[i]),
            });
        }

        return bookings;
    } catch {
        return null;
    }
}

function getUnbookedTime() {
    try {
        let timeStr = evaluateXPath(document, '//table//tr[contains(./th/text(), "verfügbare Restzeit")]/th[2]')[0].outerText;
        return parseTime(timeStr);
    } catch {
        return null;
    }
}

function setElementValue(elem, value) {
    elem.value = value;
    elem.dispatchEvent(new Event('change'));
}

let row1 = null;
let row1cell1 = null;
let row1cell2 = null;
let row2 = null;
let row2cell1 = null;
let row2cell2 = null;
let row3 = null;
let row3cell1 = null;
let row3cell2 = null;
let row4 = null;
let row4cell1 = null;
let row4cell2 = null;

function update() {
    'use strict';
    let bookings = getBookings();
    let selectedDate = getSelectedDate();
    if (!bookings || !selectedDate) {
        return;
    }
    let unbookedTime = getUnbookedTime();
    let currentDay = getDate(selectedDate).getTime() === getDate(new Date()).getTime();
    console.log('currentDay', currentDay);

    let currentWorkTime = 0/*ms*/;
    let currentPauseTime = 0/*ms*/;
    let startTime = null;
    let state = null;
    bookings.forEach(b => {
        switch (state) {
            case 'Work':
                if (b.type === 'Gehen') {
                    if (b.time >= startTime) {
                        currentWorkTime += b.time - startTime;
                    } else {
                        // might be the next day
                        currentWorkTime += b.time - startTime + (24 * 3600 * 1000/*ms*/);
                    }
                    startTime = b.time;
                    state = 'Pause';
                }
                break;

            case 'Pause':
                if (b.type === 'Kommen') {
                    if (b.time >= startTime) {
                        currentPauseTime += b.time - startTime;
                    } else {
                        // might be the next day
                        currentPauseTime += b.time - startTime + (24 * 3600 * 1000/*ms*/);
                    }
                    startTime = b.time;
                    state = 'Work';
                }
                break;

            default:
                if (b.type === 'Kommen') {
                    startTime = b.time;
                    state = 'Work';
                }
                break;
        }
    });

    let additionalPauseTime = 0/*ms*/;
    if (currentDay) {
        switch (state) {
            case 'Work':
                currentWorkTime += getCurrentTimeOfDay() - startTime;
                break;

            case 'Pause':
                additionalPauseTime += getCurrentTimeOfDay() - startTime;
                break;

            default:
                // there was no booking today
                break;
        }
    }

    let nominalWorkTime = 8 * 3600 * 1000/*ms*/;
    let nominalPauseTime = 0/*ms*/;
    let predictedWorkTime = Math.max(currentWorkTime, nominalWorkTime)/*ms*/;
    if (predictedWorkTime > 9 * 3600 * 1000/*ms*/) {
        nominalPauseTime = 0.75 * 3600 * 1000/*ms*/;
    } else if (predictedWorkTime > 6 * 3600 * 1000/*ms*/) {
        nominalPauseTime = 0.5 * 3600 * 1000/*ms*/;
    }

    let remainingWorkTime = Math.max(0, nominalWorkTime - currentWorkTime)/*ms*/;
    let remainingPauseTime = Math.max(0, nominalPauseTime - currentPauseTime)/*ms*/;
    let needPause = remainingPauseTime > 0;
    // if (currentDay) {
    //     remainingPauseTime -= additionalPauseTime;
    // }

    let leaveTime = null;
    if (currentDay) {
        if (remainingWorkTime > 0) {
            leaveTime = getCurrentTimeOfDay() + remainingWorkTime + remainingPauseTime;
        } else {
            switch (state) {
                case 'Work':
                    leaveTime = getCurrentTimeOfDay() + nominalWorkTime - currentWorkTime;
                    break;

                case 'Pause':
                    leaveTime = null;
                    break;

                default:
                    // there was no booking today
                    break;
            }
        }
    }

    let table = getBookingsTable();
    if (needPause) {
        row1 = row1 || table.insertRow();
        row1.style.backgroundColor = "#FED200";
        row1cell1 = row1cell1 || row1.insertCell();
        row1cell1.innerHTML = "Pause noch zu machen:";
        row1cell2 = row1cell2 || row1.insertCell();
        row1cell2.colSpan = 1000;
        row1cell2.innerHTML = toTimeString(remainingPauseTime) + 'h';
        if (remainingPauseTime > 0) {
            row1cell2.style.backgroundColor = "red";
        }
    }

    if (leaveTime) {
        row2 = row2 || table.insertRow();
        row2.style.backgroundColor = "#FED200";
        row2cell1 = row2cell1 || row2.insertCell();
        row2cell1.innerHTML = "Regelarbeitszeit " + toTimeString(nominalWorkTime) + " Gehen:";
        row2cell2 = row2cell2 || row2.insertCell();
        row2cell2.colSpan = 1000;
        row2cell2.innerHTML = toTimeString(leaveTime) + ' Uhr';
    }

    row3 = row3 || table.insertRow();
    row3.style.backgroundColor = "#FED200";
    row3cell1 = row3cell1 || row3.insertCell();
    row3cell1.innerHTML = "Gesamtarbeitszeit:";
    row3cell2 = row3cell2 || row3.insertCell();
    row3cell2.colSpan = 1000;
    row3cell2.innerHTML = toTimeString(currentWorkTime) + 'h';

    row4 = row4 || table.insertRow();
    row4.style.backgroundColor = "#FED200";
    row4cell1 = row4cell1 || row4.insertCell();
    row4cell1.innerHTML = "Gesamtpausenzeit:";
    row4cell2 = row4cell2 || row4.insertCell();
    row4cell2.colSpan = 1000;
    row4cell2.innerHTML = toTimeString(currentPauseTime) + 'h';

    if (!getMobileOfficeBooking()) {
        if (!getMobileOfficeReminder()) {
            // add reminder for mobile office booking
            var paragraph = document.createElement("p");
            paragraph.style.border = "2px solid blue";
            paragraph.style.backgroundColor = "lightblue";
            paragraph.style.padding = "10px";
            paragraph.textContent = "Bitte denke an die Buchung für mobiles Arbeiten auf I-100!";

            // add button to pre select I-100
            if( document.getElementById("select_auftrag") !== null) {
                var button = document.createElement("button");
                button.style.marginLeft = "20px";
                button.textContent = "I-100 ausfüllen";
                button.addEventListener("click", (event) => {
                    event.preventDefault();
                    setElementValue(document.getElementById("select_auftrag"), "I-100");
                    return false; });
                paragraph.append(button);
            }
            table.parentElement.insertBefore(paragraph, table);
        }
    } else {
        if (getMobileOfficeReminder()) {
            getMobileOfficeReminder().remove();
        }
    }
    let auftrag = document.getElementById("AUFTRAG");
    if (auftrag !== null) {
        if (auftrag.value === "I-100") {
            setElementValue(document.getElementById("HH"), "0");
            setElementValue(document.getElementById("MM"), "1");
        } else if (document.getElementById("HH").value == "0"
            && document.getElementById("MM").value == "00") {
            let timeStr = toTimeString(unbookedTime);
            let h = timeStr.split(':')[0];
            let m = timeStr.split(':')[1];
            setElementValue(document.getElementById("HH"), h);
            setElementValue(document.getElementById("MM"), m);
        }
    }
}

if( window.location.pathname === "/workflow/Projektzeit/edit.asp" ) {
    setInterval(update, 300);

    setTimeout(function () {
        let arbeitsfloge = document.getElementById('select_afo');
        let i;
        if (arbeitsfloge) {
            for (i = 0; i < arbeitsfloge.length; ++i) {
                if (arbeitsfloge.options[i].value == "120") {
                    arbeitsfloge.value = 120;
                }
            }
        }
    }, 2000); //Two seconds will elapse and Code will execute.
}


/* ********************************* */
/* **    FAVICON online status    ** */
/* ********************************* */

let favIconGreen = ' data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TRZGqQzuICGaoneyiIo6likWwUNoKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdnBSdJES/5cUWsR4cNyPd/ced+8AoVllqtkTA1TNMtKJuJjLr4p9r/AjiCGMIyIxU09mFrPwHF/38PH1LsqzvM/9OQaVgskAn0gcY7phEW8Qz25aOud94hArSwrxOfGkQRckfuS67PIb55LDAs8MGdn0PHGIWCx1sdzFrGyoxDPEYUXVKF/Iuaxw3uKsVuusfU/+wkBBW8lwneYYElhCEimIkFFHBVVYiNKqkWIiTftxD/+o40+RSyZXBYwcC6hBheT4wf/gd7dmcXrKTQrEgd4X2/6YAPp2gVbDtr+Pbbt1AvifgSut4681gblP0hsdLXwEDG8DF9cdTd4DLneAkSddMiRH8tMUikXg/Yy+KQ8Eb4GBNbe39j5OH4AsdbV8AxwcApESZa97vLu/u7d/z7T7+wHEAXLHxQCaNwAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+gDEw4kHjK2QCEAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAXrUlEQVR42s17aZAcV5Xud+69mVnVVb2oV7csGUnjdmu3GkmWtaBB0jMwRgaMzdiyiYkBhp15M0y8eG+AFzAQBMMyMATwxsM4JgzYQOCBx2LLxs/YsmQZbMtabNmSJUtWt9Td6rWql+raMvOe9yNvVWVWV0syliemO27cXKq68/vOd88599ybhDf2hwAQEYmuri7V3Nwcu/rqrnbPc5s7Oq5ISimF7/ucz+cz/f3n0iMjI2Ojo6O5vr4+j5l9ABoAm/aGPeBl/5tEJFevXh3ftGnzmvXrr9u6YMGCdUpZy4WQC5WybABCCEnMgNYarltkZq0LhYLLrAeY/Zf7+/sPHTjw7P59+/YeOXXq1DQzuwD8y03G5SRALF261Lnxxndev337jl3z5jXfmEgk52sNmpnJwfc9aM1gZkgpATC0ZkOAh3y+AN/34fs+mIH6+jpYluRCIT+SyUw+umfP4//xyCOPPNXX15sB4Bp1/JcgQGzatCl5++27buvpWfvJ5uaWVb7PolAogghIJOKwLIViPodnew9gKjGDScwgR7ngATTBgYUGTiIx7WDN/DeDhMLUVAajoykUCkU4jo3GxiQXi9njR44c/vf77vvRz06ePDkOoPh6iXg9BNDy5cudD33or27dvHnL59va2rvy+SKYGYlEHdxCDr89+xhecE7gSPw4UvOmUaxzAaLQPw+OBREEC5Am2AWFlvEGLMsuxrLpxdjUshnZnIuBgSF4no+Ojmb4fqH3wIFnv/lv//avPxseHp40iuD/NAKUUvLWW29d9pGPfPQ7S5Zc/VbX9YkIcBwLjx57FI87T+NAy0vINOfADGhXA3kNZi55ico/54AI4uA6MUHEBCzLggULiSkHq4e6cN3kcqxofTPOnj2PQqGIzs5Wzmannv75z+//3P33/+w5Zs4aH/GGEkDd3d32rl27PnL77Xd8OZmsb/A8H7at8JvnH8D9DQ/j5IJz0EJDZzVQ1GARiQcR4ILNORNIV45BAGlDiCDYCRsOO1jS34ltI+uwunkdenv7oZRCW1vjzAsvHP76N77xte+Pj4+nAHivRQ2vhQDatm1b4/ve977v3nzzLXcyg6QU2Pf8E7jX+TUOLToB7WvorA8wR4Ebq0eA68Da1WSUSQj1wYcIqk4hIeqwrO9N+BTej6GJLMbGUpg/v53HxgYf+OY3v/E/XnrppXMACpdKgrxU8DfddNP897///b+8+eZbdgJEQhD+5eDd+PbCe9HbPgQ97YFLFqdwIyPtACRpAdIiOEboGowfYDLHQU8gCHOfiwy3UMRwWxr7Y4dRl2Jc27Ea586dp8bGed1bt27Zcu7c2acHBgYmjBIuCwH03ve+96rPfe5/P7Rly1vWAUBuJoO/OfFZ/HrZPuT9PJANHHEUfAW4DAPXZJooS59MNCj1AiHwhigK/bKrkeMczi4YwVhqCNtaN2FoeBxK2Z1bt2556/j4+O/PnHk1dSkkXIwA2rlz5xV///efeWjp0uWriAj7DjyOTxe+jOcXnYKe8gDfyF1cInAWZacnNFUsH7I0cYig0i8jQgwYKBaLGOpI43T+FN5RtwVj6SloTS0bN27Yevr0K/vOnz9/USVciADaunVrw2c+89mfr1ix6jqA8fiBR/HFln9Bf+sIeNqvbXGgInc/bHGaBTwAGrJ4CDAIUQLCv1z5LLsa441TOEGn8WdqE8YnZiCEatm8eWPP4cOHHk+n09MXyhXmJGDx4sX25z//hX9et279LczA7w/uxZda/hVDjePQeb8CXMwGHsR0AWJEHnYWcAqB1lGpi9B5xfKoXKMKoXAZ6boMTuEMbrA2YmRsAvX19VeuWXNtx549j+13XTc3Fwk1CRBCiC984R/u2LHjhi8Bggr5LP6X+zWcaTsPDoOn0EOxGa9MIL8yzqPWDwHjsNUBotAxwkOJKsOrBJ8rKil9Dj4jXTeNQXcQW+MbMDA4ioULFyyfP79jdP/+/S/NFRlqEUDvete7Fn/wgx/8j1isLkEEfPbEl3Bg8cvgrD9L7mWpMiD8IJsrE1FOci4AnCvWBaKyF5jtAAVROZksDxXToIGxxkkU0tNYFV+FwcFRWrly+bqhocEnenvPjNTyB6L6wsKFC60Pf/gj/9jU1NJKBPzg4A+wZ9lh6BkvOs6pBnimUB8aq+GhwAZY6Zyi90AUqCFCrrlGkTknQGGyAzJ0QePA4pdxLHcQdXUxTE3lmv7yLz/wGcdx2gCoixFAd9xx59Zrrum+pVh0ceDIU/jxkt/Cn3FnAS89nPBDHpsDP0C+sZYffcAI8PCDIxo9qApU6Skj3ws/i3kgYZ5hppjF768+htamBKamMmhtvWLHbbft+m8AEtXJX4SArq4u5+1vf/vnmYWUUuD/2r9DOjlt8vVoghO2svCrYnvJH9DcliZCDeAIje1ycSFKlDADN+wTqDLHKmWgZxNDeEQ8hnnzGjE0NC527rzpk8lksh2ANRcB9Ja3vGVrZ+eVmz3Px55D/w9Pdb0A5HXFCuaflqWO2VIXiFq9HL8vATgRRexDYc2LUsQJnoEFBeciOC41SIBIAB7jyMJTSCrA83wkEo3Xvuc9N2+rVkGZgLa2NnXbbbd/0vdZSCnwUOOTKOhCyBolD1wa75VxHxnnmiK5/Ky8vjQvuAhwKulbECAEQBIsCSwUWElASLAUgBTBfSmDBgVIAqRASmbwYHwPGhvrMTqaphtv3HmnlLI57AvKU5YNGzYsbGlpe5vrejjw/D4cvuokWHPI20WdUomECgGiBnhUTWoqsTuc8ESBi7J1WQmwME0RICRQOhcBCVoJaCXBigwxwb2AKIFjV55FXALFoov6+sYN11+/sQuAUzJrmYB3vnPnLbFYXUwIgWcTLyFrF6LACSG5V3nokAJAJtMLze/Lnr0aOM0GrmVgca2kAS7AlixbnpWxtAyAEqlADUKZz1fugyTGnSx+L5+B4zjwfbJvuOFtNwKojxDQ0dGhuru7d2azOWjt4+mmFwCXIzG2NEkJO75KTA+B9ymaxFA03l8YeAUYRCBtlsaqYTUIacgQ0DK4BpIgkgApgGT5nEjilSuGURe3kUpNYOXKVVullE2lYSAAYNmyZc2xWN1639c4cPRJDF05UQbOAmVpl/pg7j7HOKdoqBLhcEbRMV4NXCsRAh4CL4LZVmTMG7BECoKUAVvpRZkQgbPz0nA4yIEcJ7500aJFnQBsGBaop+fN6+PxRDyfL+JU/Vl4vlcOJ+U83qdoulsey5XPlSs84cIGzaqZm3AlwBT8E13U4AKDPR+64MP3NXz2UYQPFz40AJsUFCk4ZIMUgWLSNAXEJZRSAFMo2S09C6MoGU+Lg1iqemBZjnP99RvXnD59+gUAMwqA6OnpWeu6HqQUOJk8CzbISM9OTCqzsZD1uSpk1QBfBs4MnmHoGQ/T/gzG9DQ8aHA5mIf70HSzBI4AFCmoB08ZT04CjRRDU6wessEBNVpwpFOqHgAAhptzWDmskM8XsXbtuhU//vF9cQBCARAdHR3Lc7k8HMfGufhIRao10liYWV84Uys5OIT9QUhB7DH0lIdMdgZDOgUvKPgZgCLoy86mBFpUnHCYlFBRtXSuAaRRRDo/BuQBGgbaRAL1TfUQrTHEVRzDDTNQYwKZTBYLFixYAiAgoLm5WUhpLfI8H56bwVRHNpp26mihkjicDmJ26Cvd1gBmNLITOQz4Q3DhG6Bh0CHwYUIiM5wa/UV+mBgjnMNIOgtKA52iHtycAziYEVuW3SmEiGuthWptbbUAdGit4RdyyDYUysAFAygfU82wVwatg54ZQNpHKpPGqD8OBhuQ0gAQ0fMyKaV7VUqIgA/n49V1XY72JglhZgxyBuMjGUylU6hrnAch5Ly6urpYJpMRyrZtyYxGrRnHzz8PXsGhWB/K6XUollNoihpSpZ7wMTY1hnE/HXq+avAyJPmwIqrIIBGSu6iSP81R1A6RwGZNlYLeFcAro0expumtAChh27YNQCgppRBC1vm+hiu88t8lv1SloVnDITI5AcA5xuToBM77w9GSQ0Ti1eBlbfDhIRFWRcQfXKCiXwZe4oMBDqrVrgwcvVKWo5RSAEgxBwuWWmsUqBgZ1xRWE1WxzwB5gD9UxOniWWj2q8BXW9gAFnP4gTARmGsI0CwHGIy72dIHG+uzkaxm+ORDCAEhKrJSzAzf9z2t2bKKqgIeYUuXhoBxhCBgRmN4dBRpPWlYr1pvqenwaA4nKKocYMgXRBRANXxA9WmIiAgJDOVJEBG0Zl9rzQCgisWi1lpntOb4NfXd5VUbYcZ8uZ4XqtLqYRenM33QrCvgq60fAROS/Szw1c6xmgSBqjQSkSJauRBgnJ75DUCj7AeINa6uuwZSCjD7Odd1fQCsMpmMLwRSWus2isVgFSQ8W4fGUAiUB+TPzaDPHbjQ9ojZTm7OVgIva38nNBMTRgnCrCYTolknl3lgaDA0maQLgZFsTRB1CQghQORP5fP5IgCtBgcHPdf1BrRGt1I2kqMxTM7PlZkuy67AmDg3gRFvrGLxWtZHWM41pI8LgZdVKgiACyJIEiDA9KGaoaGAwcHjEAICWEMToJmgwWjJArG6BKSUKBZzY4VCoQBAK2bWrls4pVTddiklmqYSmJyfC1ZnS8lNjjHWP4q0P3mRdaRqCYvK9Vne/sLgiSSkAS7JzERJlCvFIlJLriRAOhgA0ETQDPggaGLMn3GQSCbhODZSqekBAHkAWgDQR48ePZpMJuC6Hjpz84I/bnIAFDSGBoaMs8MFrF8je4s4Parh9WuBFxAkoUjAIgmr3EvYkFBSQFkSwiIgFm3CIigloKSADQWLBGwRfPcKN4lYzEF9fQKHDx8+BSBXJuDll48f8H1XF4suFo11Vrx/gTFybgTTOnPxxeZqx4e5vD7NdnKhJoSEVW6iAlwJIEYgW4AdADEBsgnkmGYTEBPgmDl2CFIKWAhIvCrVCMdx4HlF/eSTe4+FCeCnntr/SrGY79dao6dpIxJjDuAxUudSmOLpKPi5jlHD+qDZE5pqjx9KiISxeGD94OGlDEDBJpBF4BhAdtAHrXIMu3SPyuTAITTkBK6/YjukFJiZyYwdP378tCGABQDu6+vLZrOZPbZtwXbiuLJvHrK9GaR0elaKHQ45s8EjGrurrY8Q+LAKzPhW4QYBoQwQi8AODPgANMpWZ6B0PU7lz8AG2CHAIiyeiKGtvR2xmIPh4fNHMplMqrRUVqoJenv3PvHrxsYEF4oulg4twLA3Ooel55J/jdR1TuuHFSJAJCBJQBFFwMMSgEVly8I2lnYAxBgcZ8AO1MFxQ4YTVgRBWMDK4Q4kknWIxWzevXv3XgDlZfMSAf6DDz6wP5eb6dVaY0PHdnSlFl4Y+IXkX01K9WyvKkESxtNLEpBh8LZZxrDIADWWjXFw7LBRAAOOjt6zCWQx2kck3rbkJiilkE6PDe/e/eABU0rxwwRwb2/vdF/fmZ/Mm9cAkMCm9IryRoQLK6EG6Ah4qu0fqLL4HVhfQoEgJAGKAuAKgfQdBsfYWJ8hFSCVgCUkbJKwKTiWypBnB9+DTdjwahva2luRTNbh4MHnHp2enh4CkC2hCa8Muffe+8MfCaEnPM/H9tZ3ojv1pjlmW9V5eA3AZfmLGuqoRAlBBIlSL0GCgjUAZSxvBUDYAoQiKFECTkGUEEFvkzDXCEoG1u88q/DeBbfCtm1MTU1k7rnnngcAjJkNlrOWxvQzzzzT39d35p558xpAUmHH6JshtLiID6hVqKgmATUJIgTgpSEBEmXrs0WAYnPMkBKwiExeQLCkgC0qzZLBNctEEJsFth9diM75nairi+OBB3710PHjx04BmAxrWURsy1z4/vfv+j9C6EFmxuar3oFtg2svvtOOqiYmhAtMZSm0X7C0FG5WjgXAkgOBKA5IkASSKDtJi0oWN70QRgnSJD5BFFnxXAK3rr4Dtm0jlRod/+lPf3o/gKHqjRLVy+P60KFDg88884evtLc3s+f5+IvELiya7ryAA7wUJdT+HJVT3KAPwBJYASTMMFBcBi9JQAkJJUq5QkCIXSLGfK5xmPDp4p1oaZkH25Z8993f/1FfX99J4/31hfYHMIDCd77z7Z+Njw89kkzWQcYSeN/YNtS7iYtYH1HJE2qoAZHqTgU8wGamzIKDRRPJgGCQCPkIEQwXVRr/JdlLA14QnDzh3fuWYkn3NXAcG0eOHHr2Jz/5yW8ADNbaJlNriwzn83l3cHDg+R07tt+Uy7kNVzb9CZz+PF6qPwNNukbhg2qUu6h2/c+0SuyXQfiTAmwBZAFsmwhgM6QgKBFYuGR5JQhKwFwjKDN8hEvYvHs+PrDxw4jHYxgePj/6t3/7N18aHh5+wTg//1J3iXF/f/9UMpk4tWHD+p0TE9PW0paVkOezOJbsDRZOZhFQo9RdXQwJkUJEkEIai8py+KNSCLQBkgQhA6lLKYNEScCQIIzVg+EiPGDNw+345JqPo74+iVxupvD1r3/ta3v2PL4XwEDY81/qPkH/0KGDA0uWLEmvWLF8R3piWqxuWQM1kMfLyV6jhIsUP0T1OUUUoIwKJAkIiSDltUwkUGapXwQOUAqCFCZhksEwUBIBCUWBVQ924L+v+RSamhrhukX/q1/9yl0//OEPfwmgNxz3XwsBDMDbt2/viWXLlnpdXddsSU9M0erWNWgaBE47g8jLwoUVgLlVQSQiGaCQIlCAMPFfAZAU7H0gAx4oh0whAh+iMgLrH1iAT238azQ2NsB1i/pb3/qnH9111133Ajhlsr7XvlEyFBrdffv2Hu3u7sovW7Z0cyo9Ja5pWYGrh5owotMYjU9e4hCIrvgQmfhPMnCEUoAlgiGgjEO0AsBEAXghqLxTRhCh/ozCzr09+NANH0V9fQK53Iz/1a/+4w++973v3cPMJa/vv97N0lprXdizZ8/R+fM7z69atXLLzEzObqlfgBuSW1EcnkBvbBie1FXAaY7j0NwfFQUQBSEwLP8gLHKwOYOC0pggQOYJK//Qis+pT2DLuj9FLOZgfHws98Uv/sP37r777h8z8ysA0pdjs3SZBGYu7t//5AnPc4+sXduzwbadpsxMHte3b8Q155ug8y7OxVPB9paI/KuICPkNUVYBBcVKadLg0manUmJEEgIMoYGrjtfhz59bj4/1fBitba1QSuLkyRODf/d3n/7KL37xi98AOG2yvcu2Xb5MAoDiiy8e7X/++cO/u/baVa0LFszvnpjIiNbkldjW9qdYMpiEKmgM2RPwJGYvc9eoCpVyAckisDiZlFgE32cJWEWg+0QD/vxwDz69+KNY09UD27ZRLBb073736GOf+MTHv3zo0KGnAPQBmH4tr878Me8MCQCOUqrlAx/44Dt27nzX/4zHG7pSqQkopVBfn0BxKo1f5B/CC8VXcax5HHkL5R0dld0dwQ6O0ozOIhGUvcz83mLCFSkb1+UWYf10F7as3Q6lLLO0JfHqq6f7vv3tb93zq1/96jGtdS+AcZPovKa3yP7Yt8ZMSRfJ9vb2Kz/2sY/fdt111/9FPF7/pnR6CszB63KxmI1CZgpPTfwBZ+MjOOeNYtyfxqTtIi8DqQuSiGuJBFlothNYzB34k/wV2Na6GY0tbZBSgQhQKtgFMjh47vzDDz/8y+9+9zsPTE5O9gIYDlmd/xggr+udQVOyaGhtbe3ctevOd2zfvv2WRKKhRynbmpzMgIiglEQ8HoPjWFBKwXNdaO0HcwEhoJSCE4tXFjY0m+8JxOMxZLMz3vnzA8d279792/vuu3dPOp0+B2DEhLjX9RLl5XpzVJhNRwmlVMu1117b9e53v2fHqlWrN8XjieWxWF3SsmzK5wvwPK8MPNykFHAcB/G4A89zOZOZnpmcnHjlkUcePvjEE0/sP3r06Cmt9Yjx7pnL8dLkG/HucGknbwxAQgjR2Nra2tzd3d3V07O2e8WKFVe1tDS3C6EabdtJSCkEM7PrFrOe502l06mxF198sf+55w68evz48dOpVCrNzJPG0hkzxr3L9drsG/XydHgeLM0QcaqaVdqhFs46jZwLVS380jS/EQ/6n/FDVU3Msb0j/Lr8G/rafOnn/wMAv+nTPttKGwAAAABJRU5ErkJggg==';
let favIconRed = ' data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TRZGqQzuICGaoneyiIo6likWwUNoKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdnBSdJES/5cUWsR4cNyPd/ced+8AoVllqtkTA1TNMtKJuJjLr4p9r/AjiCGMIyIxU09mFrPwHF/38PH1LsqzvM/9OQaVgskAn0gcY7phEW8Qz25aOud94hArSwrxOfGkQRckfuS67PIb55LDAs8MGdn0PHGIWCx1sdzFrGyoxDPEYUXVKF/Iuaxw3uKsVuusfU/+wkBBW8lwneYYElhCEimIkFFHBVVYiNKqkWIiTftxD/+o40+RSyZXBYwcC6hBheT4wf/gd7dmcXrKTQrEgd4X2/6YAPp2gVbDtr+Pbbt1AvifgSut4681gblP0hsdLXwEDG8DF9cdTd4DLneAkSddMiRH8tMUikXg/Yy+KQ8Eb4GBNbe39j5OH4AsdbV8AxwcApESZa97vLu/u7d/z7T7+wHEAXLHxQCaNwAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+gDEw4gKJlgELwAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAVgklEQVR42s1be2xc1Zn/ncd9zMMeP2IndhwIIcEmjkncJCSBNEDShELStN2WRQsVolVh1U213Urd1a66YquqTXcrVay2AXZXlLBAW0ER1ZYC5VEeTaCQkHdIIA/b8fsxHs+MPY87995z9o97Z+bO9cw4KaFaS0f3zJ074/n9vt/3fed85xyCT/aPACCEENrV1cWbmpr0lpbW5tramgZFUcOMMWrbtsxmszPT08mp4eHh6MjISObMmTOWlNIGIABIt31iP/CyfychhK1bty7Q3f2pVWvXXr+pra1tDefKckrZIs4VFQCllBEpASEETDMnpRTCMAxTSjEkpf3h4ODg4YMHD+w/cOC9oydPnpyWUpoA7MtNxuUkgK5atUrbvHnL+s2bt/xVfX3D7aFQuFUIkFQqA9u2IISElBKMMQASQkiXAAvZrAHbtmHbNqQEamqCUBQmDSM7PjOTePWNN17/1csvv/z2hQt9MwBMVx3/Lwig27ZtC+/Y8bk7u7tX72poaOyybUkNIwdCgFAoAEXhyGUzsJIJqJkM7EQcMpMFAEhIEE0Hra1FRtUggiEQypFMzmBiIgbDyEHTVEQiYZnLpU8fPXrkZ88++8zTx48fnwSQ+7hEfBwCyOrVq7W77rr7yzfeuPGBpqbmZdlsDlJKhEJBmEYGuYELECdOgB0+jNpEApptgxDidRbnQikkpZCEIMc5EpEIrOWdsK5pR65hHtIZE0NDo7AsG/PnN8C2jb6DBw/8ZO/enz09ODiYcBUh/2wEKIrC7rvvvmu//OU7/mPJkqU3m6ZNCAE0TcH0QD9yb7yO2vfeQ9g0ASGAbBYyk3H6HuAgxPnVhEAS4lwpBQkGQUIhQFWR0jTEOzthda9BVg+hv38EhpFDS8s8mU4n33322We++8wzT78vpUy7MeITJYCsXLlS3bVr1/2bNt38g3C4ptaybKgqx+SZjyCeeRrze3pAbRtyZgbSMIoWdwF6gRdA5/u+eyAE4By0oQFS1zHR1obMp29CJhhBX98gOOdoaoqkjh8/8uMHH/zJf42NjcUAWJeihkshgOzcuTNy//33/3T16rV3SwnCGEVibBSpp55Ey/HjIIYBmUwCUpYC9xCQB1uJgIIafPdACEgkAhKJYOzqpchuvAmj8TSi0RhaW5tlNDr8/MMP7/nO+++/PwDAuFgS2MWC/8pXvtL6rW9969fd3at3AIRQSjD4+muo+fcH0djfDxmLAdksCCEOeF+T/kZpyf28/KXn+VmfNQyI6WmEkwkEe84BLc0ILVqEgYEREonUt2/YsH5jLDb5bm9vb9xVwmUhgHzta1+7Yteub77Y2dm1BgAyqRmM/OcjaHvxRSixGGQqlR8AlAfuAqvU4LuWgHeJKvlOwwBJJlEXnYBl5VDT2YnRsUlwrrZs2LDu5snJyXd6e3tiF0PCXASQu+++e8E3vvE3L3Z0LO8ihGByZATT/7obrSdPQE5OAm5kJ74fWhF4BcujDHDpJ9T7P6SEmJlBTSIOEo8hcN11iE4lIQRp3LBh3aaRkaE/9Pf3z6mEagSQ7du31+7a9c1nOzu7rgckokODsH74Q8y70AcZj88pd1HB0hWtXeZ7MEcThoFAMgk1EYPStQKT8RQo5Y3XX7+m+9SpD16PRqPT1cYKFQno6OhQv/Odv39wzZq1X5ISmBofh7X7B2gYGABmZorAPVaRc1h+FvByPl9GDSjjBiX/1zShJpNQUtNgnZ0Yj8ZRU1OzsKtrxfy33npjfy6Xy1QioSwBlFL63e/+811btmz9PkCJkU0j/eBP0HT+PJBKlbWErGL5Stb3A5cVvpdUUIbXCNK2oSYSYLYJ1tGBoeEJLFrUtrylpXli//79H1TKDOUIIPfcc89Vd9111690PRgiBBh74nEsPHQIcnq6KnhRxfKVgFeV/RyWh58cIRBIJGCHApCtbRgeniArVixfMzo6/GZfX+94uXhA/Teuvvpq5Y47/vJHdXWN8wgBht55G1e8vR8ykSj7Q/xWF1VUUI0UWcYN/O4yl/JACEQmg/nHjqAum0QwqCOZzNTde+9X/0nX9SYAfC4FkPvv/+tbtm7dtlsI0FRiCpGH90AdHy8OSHw/Tlwmn68W+EiZ90gVUpDJIGBkkWtvx8RkAi0tC64C5Kljx46ec12hvAK6urq0W2+99QEpKWOMIv38b1AzOQlIWZZ5r7XFJUR9WcWS5SQvfX5PKgH3kKX196NhoBf19RGMjk7SHTs+t6u2trYZgFKJAHLbbbdtamlZeKNl2Yj296Ht3T86k5gqlhdV5F1J8uVcCNUsWgDueU19jVHnSghACaRtY94HJxDmgGXZCIUiK3fu/PwtAELeKUCBgJaWFr5ly2d22bakjFHglZfBk8k5LV91dFdhaHtRwCkFCuAIwFyQnIBy5x5hBITTYp8RgLvPMQIai2LeQA8ikRpMTEyR22/fcTfnvMEbC/IEkM2bNy9qbGzaZpoW4uOjaDl6BLCssj46l+UrpbyKI7wS4EWrUg8wb98B7jTKHELyV+/7lBE0nj+DAANyORM1NZF169atXwZAy6ugQMDGjZ/+kq4HdUopcPwYtDL5XlTx6ZJIfLF53kuuD3QejAOsaHnqAs9bGdxDCCslhDACnphEYyIKTdNg20TdunXb7QBqSghoa2vj7e3tO9LpDISwEXx7P6RpVrW8qGB5UWE0J+ewOGUeYD5LllNAweol932fdVvdQA+CARWxWBwrVnRt4pzX5d2AAkB3d3eDrgfX2rZAYmwETVOxkuBTydK4iBldtUmNH/gsmbO8O8CNAUXLe8kpxAHuI8P9fHh8CJp0xkCaFui45pprWgCocFkgS5cuWxsIhALZbA7KwABoOl0oaBQGOr5ChvRWbTz34LuHCoURQuEAg4Q0DNhGFtIyIbMZSDMHaVmAaQBWDpA2iKqDKBxED4BwDhoMguo6aDAApuugmlasJMHzf0FA7BzqUknEeRiKomnd3Z9aderUqeMAUhwA7e7uXm2aFhijYOfOgghRWr2p1PdcZ5W8ygEHACFgZ1KwZ6Yh4lMQk6OAbYIQ6TzihgUQl6D87Jc4A/n8e97iH6EMJFwP1twK1tAAHomABoPFh0EQTkyAz69DNpvD6tVrOn/+86cCACgHQOfPn788k8lC01SEes6VyrtKCUv6Sl3e90oUZFkwk0lY0QnYIwMglllUgHsl+XhIZ4P3Xr1cFrO5DaSjsHujsHsBAwS0vgV84ZVQmpvBamoQTEbBW9sxM5NGW1vbEgAOAc3NzZQxZbFl2bDMGcxPp0sjvK9gKSvU+LySJ+60y0qlkBsfh9XbC1g5ByRzGy1tedAlfVKZhIrVTAkQKSETwzDjwzA/IKCNV0B2rgLaN7hVbbWFMRawbZvyBQsWKADmCyFgGxkETdMJUhV8vlIcKKhBSuQSCRj9/bBHRpwCKS0D2v+aeNRQTQFe9/YSIYtXCYC4K4pSSsjYBYi3x5Dc+AUEI/WglNWHw2E9kUhQrmkakxIRISREOg1YVllgskJ523s1Ewlke3pgj415/NMHtoz1K6khDzbfB+ZQgUsCkc70xVGD06fIQcxMg9Q1AiAhTdNUAJQzxiilLGjbAsSyZtXt57I8ANiZDDK9vbD6+0sqDrPAXSwR3mBYzhWquEABuKcPAVBIUMsJ9JwrGuecAyBcSmfBUggB4g5+8p+btUjhs7gQAsbwMIzTpyFtuxQ8KQ+eViDhkoIhShUhfdbPE5G3PqHOohSxbVBKQWmeWpcA27YtIaRSqOj4I38ZMqxUCpnTp2FPTMyuM5GLsHSFmFDy2UoK8JOAUv/3St9LgsUYCCEQQtpCCAkA3DAMIYSYEUIGLMqqRv781RgbQ+bYsRKr+62PaqDncIW5g2GRBSfdSkfucDIApCwFLwEpKYSigjEKKe1MLpezAUieTCZtShETQjQRXYdJKXi+AOKTvhQC6Z4emGfPVtkdcRHWrkYIrZAOKXWLHRSEeCZRrv0J8sAlIAWIFJBSgAiHDAENNBgCpRSE2Ml0Op0DIHhfX59lmtaQEGjnXEWSK6i3zFmRX5gmUqdOwRoaKsaIMtZHFUs6Y/pLUEMeOGUgbvBwri4R3kwkpbPXwCVACgEibUghAGFjRm2GHgyBMYZcLhPNZrMGAMGllMI0jXOcBzczxpCsq0f95ESJz9uGgdSxY7Cj0erLSLT6AOeSsgIjIJS74DlAmVsKyyuA+gYEKIKXAkQISGkDwgYRNpINixEKh6FpKmKx6SEAWQCCAhAnTpw4EQ6HYJoWUlcsLlmqEoaB9KFDENFoieVnWZ+UqpJ4c/gceb9EDQzuTE4B4SoI10C4Cur2KddBFN3ta6CK27jmPuv2FU+fq0guXAZd11BTE8KRI0fOAcgUCPjww9MHbdsUuZyJZOvC4gwwl0P68GGIeHzOtWZ/4MNcOb4MCZQClDFQFzBlqoeEPDD3PSUPUneaUiTDeTZPmvP52MKroWkaLCsnDh9+/5SXAHnw4IGzuVx2UAgBs7EZM5RCWhYyx45BTE2VgK/URxnrl+1XdQPmWJ4pIC54yrxA1IKlqVcB3KcAxasGFSm1HuzKJWCMIpWaiR4+fPi8S4CkAOTZs2fT6fTMG6qqQNUCGJrf6uT48fESqZcNemXAV1JC9QBJQRj3EKB43EAtAVmwvlIkgfByruB8drClG03NzdB1DWNjI0eTyWQsv1SWrwlab7315v9GIiFp5EyMLeuANThY3tKV5E9mD1zKqsKXwRzwxAFP/eDzrZIC9CIRStH3vfIH0zBy7VqEwkHouipfeOGFtwAUls3zBNivvPLy/kwm1SeEAF20GONd11UFXk3+s6Tvs/jsGOBEekcBvAieqW7zB0SvlXWHCK57gp7zHGEqxgLzUXNtFzjnmJqKjv3udy8dBJDMb6jKEyDPnDkzfeFC7y/q62sBQjF8++c9JaYq+1VJlXl7FWKKGYK4vs8Byj1EOCqgTHHiACtmAuKJ/kTxvS4oxbme7bwFTc3zEA4HcejQ+6/G4/FRAOk8HO/KkPncc88+QamIW5YN2bECEytXVbc8ijOziuDLjeWpxxUoc4G7JORVQH1qcF2hkAF8gZD4giNhKka1Ruir10NVVSST8Zm9e/c+DyDqbrCctTQmXn/99cELF3r31tfXgjCO/i/eCcFY9RjgnZiQMqqoqhICEAZCmDvQYaXuUKIGtZAZCFNLcn0JcNfykqs4veozaGltQTAYwMGD7754+vSpcwASXjvSkqm0lMaTT/7PQ5SKYSkltGUd6Nv5xTk32hFfsYLM4RpFlbgLnZQ6JJSQwYuK8BNRGBRpoIruk78GwhScjlyJBdffAFVVEYtNTD700EPPABj1b5Tw7w8Q+/fvH37vvT/ubm5ukJZlY3r7FxC/pr2qG1RTAvzlqxKVlA5tHTL8ivCC5yBcKRngOE33BD4Fk1RH/JadaGysh6oy+dxzzz5x9uzZM270F9U2SEgAxsMP73l6cnL05XA4CKaHMHDfLuTq6qoHQHiu3mpZNZVQH/g8GYX77iQoT4Y3OObHBkpR+pSryFEFJ9ZsxZL2a6BpKo4ePXxgz549vwEwXG6bDC1jSzExMZF47LFH/1HX2RAAqG2LceYbfwtbVS9uz2k1BRSPURSYISVr+3S2MvLu4QmW+dRYzP0abKrg3cXXYekNN0LXNYyPj0786Ee7f2oYRq+b+sTF7hKTPT09yXA4dG7durU74vFpRV+4CNHWFtS/f8BZOEGFmV6ZuT31TXioO/ihjLmDH5+fUw7qZgPq3qO8mBYd4Irr7yoIZbAl8G7jQrTdth01NWFkMinjxz/+t3979dVX3gIw5I38F7tP0D58+NDQkiVLpjo7l2+Zik9T/aqlGG9biLpDB0Fte86pLvXXA8tNd/PgXSJoSQZQSp6hbhwokMIUEEWFBYJ36puxcPtO1NVFYJo5+9FH//uRRx555NcA+rx5/1IIkACsffv+8FFHR7u1bNk1G6fiSRK8aikmli9H+ORx8HR67spuxUZ8Kc8zGCoQkJe9mxEoA2HFmADKkBEC7yxcjKtv34FIpBammRNPPfXEE9///vefBHCukvQvdquslFKa+/b94UR7+9Lstdd23BibSlK9ZSHiG24ARoYRHB2++Dp/SaHTJYB5ApzndREkB2G0+DqfKgnFiAA+WLsRnZs3o6YmhEwmZT/22M8ef+CBB/ZKKfNR3/64m6WFbdvGm2++eaK1dcFIV9eKjalURoUWhLzpZkxF6lHz0Qdgtjmr/DVXvwCKsdIRYaG55bB8KczNDjkAJ+e1Ibv9L7Csqwu6rmFyMpp56KE9e3bv3v1zKeVZAFOXY7N0gQQpZW7//n0fWZZ5dPXq7nWqqtXNpLIIdq3E1I03I2tYCA+cB82v8noKmsS/1lcgIQ+QlQB36oDF4EGoIx9BKPqCDei9aTtaN2/DvKZ54JzhzJmPhr/3vX/Z/eijj/4GwHl3tHfZtssXSACQO3nyxOBHH51+rbPz2nltba3t8fgMhR6AeuOnMbl+E9JSQWD4Arg0yy5zlxJBSvy5OBZghShKCEGOMFxoXIy+TTtRe+tOzL/ySqiqilzOEK+99urvv/3tv/vBvn373gZwAcD0pRyd+VPODFEAmqIojffe+9XP7tix8x8CgdplsVgcnHPU1ISQS06BnToB7c2X0dR3GKpMOytC3F0ZYu6VE0+tzzON5RosJYCJ2jakP3UzxIpuNLVdAc4Vd2mLoafn/IXHH39s7+OPP/57IUQfgEl3oHNJp8j+1FNjxFVPuLW1deHXv37fnddfv/6eQKDmyqmpJKR0jsvpugpjJgkWHQMbuABy/kNogz3QMjFwmXHHBxRCDcEKNSDXejXkkg6IRVcBLW2INDaBMe4eHWLgnGN4eGDkpZde+vXDDz/0fCwW6wMw5rG6/FOAfKwzg+7Oy9oFCxa03HHHnZ/dvHnzl0Kh2m7OVSWRcLbVc84QCOjQNAWcc1imCSGcgxaUUnDOoemBfNqBENL9HEUgoCOdTlkjI0OnXnjhhd/98pe/eCMajQ4AGHdT3Mc6RHm5To5Sd9NRSFGUxvXr1y/bunXblq6u624IBELLdT0YVhSVZLMGLMsqAPc2xig0TUMgoMGyTDkzM51KJOJnjxw5dOi3v/3t/gMHDpyzbXvcje4zl+PQ5Cdxdthd+4EOIEQpjSxYsKBh5cqVy5Yta2/v7Oy8orGxoZlSHlFVLcQYpVJKaZq5tGVZyampWPTkyZODH354uufIkSPnx8fHp6SUCdfSM66PW5fr2OwndXjaOw1irotovqbkd6h5R52unA1f8x6alp/ED/1z/BFfoxX2dwjf4pP8pH/Y/wFiSX4FHH6iwwAAAABJRU5ErkJggg==';
let favIconDefault = 'https://zeit.spangler-automation.de/workflow/images/OutEnabledR40.png';

let onlineStatus = await GM.getValue("onlineStatus", null);

// Add events to login / logout button
if( window.location.pathname === "/workflow/AnmeldungZeit/anmeldung.asp" ) {
    setTimeout(function(){
        if (document.getElementById('B2') !== null) {
            document.getElementById('B2').addEventListener("click", async () => {await setStatus(0);});
            if(onlineStatus !== 1) {
                setStatus(1);
            }
        }

        if (document.getElementById('B1') !== null) {
            document.getElementById('B1').addEventListener("click", async () => {await setStatus(1);});
            if(onlineStatus !== 0) {
                setStatus(0);
            }
        }
    }, 500);
}

// Add Favicon
var link = document.querySelector("link[rel~='icon']");
if (!link) {
    link = document.createElement('link');
    link.rel = 'icon';
    document.head.appendChild(link);
}

async function setStatus(status) {
    await GM.setValue("onlineStatus", status); // save login / logout

    setFavIcon();
}

async function setFavIcon() {
    // delete old favicon
    var oldLinks = parent.document.querySelectorAll('link[rel*="icon"]');

    for (var i = oldLinks.length - 1; i >= 0; --i) {
        var ln = oldLinks[i],
            pn = ln.parentNode;

        pn.removeChild(ln);
    }

    // set new favicon
    switch(onlineStatus) {
        case 0:
            link.href = favIconRed;
            break;
        case 1:
            link.href = favIconGreen;
            break;
        default:
            link.href = favIconDefault;
    }

    parent.document.head.appendChild(link);
}

setFavIcon();
