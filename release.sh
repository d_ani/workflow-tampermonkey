#!/bin/bash
set -e # exit on error

# source: https://stackoverflow.com/a/57800614 with my modifications
# we need to extract the ssh/git URL as the runner uses a tokenized URL
export CI_PUSH_REPO="$(echo $CI_REPOSITORY_URL | sed 's/[^@]*/git/' | sed 's#/#:#')"
git remote set-url --push origin "${CI_PUSH_REPO}"

git add -f ./dist/meta.js ./dist/user.js
git commit -m "Update release artifacts for $CI_COMMIT_TAG"
cat "$SSH_PRIVATE_KEY" > .id_ed25519
chmod go-rwx .id_ed25519
git -c core.sshCommand="ssh -i .id_ed25519 -o 'StrictHostKeyChecking no'" push
RELEASE_COMMIT_SHA="$(git rev-parse --verify HEAD)"

release-cli create --name "Release $CI_COMMIT_TAG" --description "New release" --tag-name "$CI_COMMIT_TAG" --ref "$CI_COMMIT_TAG" --assets-link="{ \"name\": \"meta.js\", \"url\": \"https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/$RELEASE_COMMIT_SHA/dist/meta.js?inline=false\" }" --assets-link="{ \"name\": \"user.js\", \"url\": \"https://gitlab.com/d_ani/workflow-tampermonkey/-/raw/$RELEASE_COMMIT_SHA/dist/user.js?inline=false\" }"

echo "Finished creating and uploading release"
